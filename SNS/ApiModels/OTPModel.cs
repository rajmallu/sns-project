﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.ApiModels
{
    public class OTPModel
    {
        public string Email { get; set; }
        public string Category { get; set; }
        public string Password { get; set; }
    }
}