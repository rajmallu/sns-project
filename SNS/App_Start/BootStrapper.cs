﻿
using AutoMapper;
using SNS.AutoMapping;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace SNS.App_Start
{
    public static class BootStrapper
    {
        public static void Run()
        {
            AutoMapperConfiguration.Configure();
        }

    }
}