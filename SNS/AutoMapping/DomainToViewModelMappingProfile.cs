﻿using AutoMapper;
using SNS.Models;
using SNS.ViewModels;

namespace SNS.AutoMapping
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {

            CreateMap<SalesAgent, SalesAgentVM>();
            CreateMap<Customers, CustomersVM>();

        }

    }
}