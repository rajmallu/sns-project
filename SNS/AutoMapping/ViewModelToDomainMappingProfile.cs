﻿using AutoMapper;
using SNS.Models;
using SNS.ViewModels;

namespace SNS.AutoMapping
{
    public class ViewModelToDomainMappingProfile : Profile
    {

        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            CreateMap<SalesAgentVM, SalesAgent>();
            CreateMap<CustomersVM, Customers>();
        }


    }

}








