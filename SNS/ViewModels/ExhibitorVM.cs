﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace SNS.ViewModels
{
    public class CustomersVM
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "*")]
        [DisplayName("Theatre Name : ")]
        public string TheatreName { get; set; }
        public string Category { get; set; }
        [DisplayName("Address : ")]
        public string Address { get; set; }
        [DisplayName("County : ")]
        [Required(ErrorMessage = "*")]
        public string Country { get; set; }
        [DisplayName("State : ")]
        [Required(ErrorMessage = "*")]
        public string State { get; set; }
        [DisplayName("City : ")]
        [Required(ErrorMessage = "*")]
        public string City { get; set; }
        [DisplayName("Zip : ")]
        [Required(ErrorMessage = "*")]
        public string Zip { get; set; }
        [Required(ErrorMessage = "*")]
        [DisplayName("Threate Type")]
        public string TheatreType { get; set; }
        [Required(ErrorMessage = "*")]
        [DisplayName("Screen Count : ")]
        public int ScreenCount { get; set; }
        [Required(ErrorMessage = "*")]
        [DisplayName("Ticket Price")]
        public decimal TicketPrice { get; set; }
        [Required(ErrorMessage = "*")]
        [DisplayName("Email ID : ")]
        public string Email { get; set; }
        [DisplayName("Mobile : ")]
        public string Mobile { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}