﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace SNS.ViewModels
{
    public class SalesAgentVM
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "*Please enter name field")]
        [DisplayName("Name")]
        public string Name { get; set; }
        [EmailAddress(ErrorMessage ="Please enter valid email address")]
        [Required(ErrorMessage ="*")]
        [DisplayName("Email ID")]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "*")]
        [DisplayName("Mobile Number")]
        //[RegularExpression("([0-9]+){10}", ErrorMessage = "*")]
        public string Mobile { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(255, ErrorMessage = "Must be between 5 and 50 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [DisplayName("Create Password")]
        public string Password { get; set; }
        [Required(ErrorMessage = "*")]        
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }
        public string CreatedOn { get; set; }
    }
}