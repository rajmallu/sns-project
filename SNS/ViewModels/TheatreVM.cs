﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.ViewModels
{

    public class TheatreVM
    {
        public AboutTheatreVM AboutTheatreVM { get; set; }
        public AboutInventoryVM AboutInventoryVM { get; set; }
        public AboutRevenuesVM AboutRevenuesVM { get; set; }
        public AboutOtherThingsVM AboutOtherThingsVM { get; set; }
        public AboutTechnologyVM AboutTechnologyVM { get; set; }
        public AboutLocalInsightVM AboutLocalInsightVM { get; set; }

    }

    public class AboutTheatreVM
    {
        public string TheatreName { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public string Strength { get; set; }
        public string Bussiness { get; set; }
        public string OtherAmenities { get; set; }
        public string Ownership { get; set; }
        public string ContactDetails { get; set; }
    }

    public class AboutInventoryVM
    {
        public string TotalCapacity { get; set; }
        public bool Stall { get; set; }
        public bool UStall { get; set; }
        public bool DressCircle { get; set; }
        public bool Bolcony { get; set; }
        public bool Box { get; set; }
        public string SplEventPrice { get; set; }
        public string AvgPriceRealization { get; set; }
        public string PercentageWalkVsAdvance { get; set; }
        public string EvenShow { get; set; }
        public string MinRentExpectation { get; set; }
    }
    public class AboutRevenuesVM
    {
        public bool FB { get; set; }
        public bool Parking { get; set; }
        public bool LocalAdvertising { get; set; }
        public bool Other { get; set; }
        public string OverallRevenues { get; set; }
        public string AdditionalRevenues { get; set; }
        public string TaxRate { get; set; }
        public string AvgAnnualReb { get; set; }

    }
    public class AboutTechnologyVM
    {
        public bool ScreeningSystem { get; set; }
        public bool InterestInDigital { get; set; }
        public string BookingSystem { get; set; }
        public string PaymentType { get; set; }

    }
    public class AboutLocalInsightVM
    {
        public string SuccessfulGenres { get; set; }
        public string BestReleasePeriod { get; set; }
        public string WorstReleasePeriod { get; set; }
        public string ListSuccessfulStar { get; set; }
        public string AboutRegionalFilms { get; set; }
        public string AboutEnglishFilms { get; set; }
        public string AboutDubbedFilms { get; set; }
        public string UniqueCases { get; set; }
        public string BusinessInsight { get; set; }
        public string FutureAssessment { get; set; }


    }
    public class AboutOtherThingsVM
    {
        public string AboutPublicity { get; set; }
        public string AboutPosters { get; set; }
        public string AboutDistributors { get; set; }
        public string AboutProducerDeals { get; set; }
        public string AboutOccupancyLevels { get; set; }

    }
}